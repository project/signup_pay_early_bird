Module installation
===================
Follow the standard drupal installation procedure and copy the module files to 
sites/xxx/modules/signup_pay_early_bird/

You should then be able to enable the Signup pay Early Bird module in the
admin interface.

Another option will then be available below the standard signup pay optoions,
which allows you to set a percentage, or a fixed amount of discount, and the 
end date at which point the Early Bird discount ends.

Once entered, this is taken into account for all signups.

Depends on the patch at http://drupal.org/node/263575 for signup_pay module
